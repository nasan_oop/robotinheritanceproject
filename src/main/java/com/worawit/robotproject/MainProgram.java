/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.worawit.robotproject;

import java.util.Scanner;

/**
 *
 * @author werapan
 */
public class MainProgram {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        TableMap map = new TableMap(17, 15);
        Robot robot = new Robot(2, 2, 'x', map,20);
        Bomb bomb = new Bomb(5,5);

        map.setBomb(bomb);
        map.addObj(new Tree(10,10));
        map.addObj(new Tree(11,10));
        map.addObj(new Tree(13,11));
        map.addObj(new Tree(16,14));
        map.addObj(new Tree(9,11));
        map.addObj(new Tree(13,9));
        map.addObj(new Tree(10,12));
        map.addObj(new Tree(11,12));
        map.addObj(new Tree(5,6));
        map.addObj(new Tree(4,4));
        map.addObj(new Tree(4,5));
        map.addObj(new Tree(6,5));
        map.addObj(new Fuel(4,8,20));
        map.addObj(new Fuel(7,3,10));
        map.addObj(new Fuel(5,2,12));
        map.setRobot(robot);
        while(true) {
            map.showMap();
            // W,a| N,w| E,d| S, s| q: quit
            char direction = inputDirection(sc);
            if(direction=='q') {
                printByeBye();
                break;
            }
            robot.walk(direction);
        }
        
    }

    private static void printByeBye() {
        System.out.println("Bye Bye!!!");
    }

    private static char inputDirection(Scanner sc) {
        String str = sc.next();
        return str.charAt(0);
    }
}
